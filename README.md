# Arma Reforger Docker

## Usage

```
    docker create \
        --name=arma_reforger \
        -p 2001:2001/udp \
        -v path/to/config:/configs \
        -e ARMA_CONFIG=server.json \
        arma_reforger
```
Make sure you point the image to your config path and put in your configs name in the ARMA_CONFIG environment variable.

## Server Configuration

[Example Configuration File](https://gitlab.com/Hamster2k/arma-reforger-docker/-/blob/main/server_config_example.json)

You need to keep the ´{SERVER_IP_HERE}` in the config file as it will be replaced with your local docker instance IP by the docker container, otherwise people will not be able to join our server.

`gameHostRegisterBindAddress` needs to be set as your server's external IP address.

Change the value for `scenarioId` from `"{ECC61978EDCC2B5A}Missions/23_Campaign.conf"` to `{59AD59368755F41A}Missions/21_GM_Eden.conf` to change it from the gamemode Conflict to Game Master.

## Issues

Please let me know if you encounter any [issues](https://gitlab.com/Hamster2k/arma-reforger-docker/-/issues) with this image.

## Newfound Pioneer Corps

This docker image was made for the Newfound Pioneer Corps arma community. Looking for people to play arma 3 or reforger with? [Join us!](https://discord.gg/dkHsgTs)
